import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { GeneroProducto } from './../models/genero-producto.model';
import { ProductosApiCliente } from './../models/productos-api-cliente.model';
import { Store, State } from '@ngrx/store';
import { AppState } from '../app.module';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ElegidoFavoritoAction, NuevoProductoAction } from '../models/genero-producto-state.model';

@Component({
  selector: 'app-genero-lista',
  templateUrl: './genero-lista.component.html',
  styleUrls: [ './genero-lista.component.css' ],
  providers: [ ProductosApiCliente ]
})
export class GeneroListaComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<GeneroProducto>;
  updates: string [];


  
  constructor(public productosApiCliente: ProductosApiCliente, public store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    //this.productosApiCliente.subscribeOnChange((d: GeneroProducto)=> {
    this.productosApiCliente.subscribeOnChange((d: GeneroProducto) => {
      if (d != null) {
        this.updates.push('se ha elegido a' + d.nombre);
     }
    });
        
 }

  

  ngOnInit()  {
  }
  
  agregado(d: GeneroProducto)  {
    this.productosApiCliente.add(d);
    this.onItemAdded.emit(d);
    this.store.dispatch(new NuevoProductoAction(d));
  }

  elegido(e: GeneroProducto) {
    this.productosApiCliente.elegir(e);
    this.store.dispatch(new ElegidoFavoritoAction(e));
    

  }
}



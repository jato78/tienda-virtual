import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormGeneroProductoComponent } from './form-genero-producto.component';

describe('FormGeneroProductoComponent', () => {
  let component: FormGeneroProductoComponent;
  let fixture: ComponentFixture<FormGeneroProductoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormGeneroProductoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormGeneroProductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

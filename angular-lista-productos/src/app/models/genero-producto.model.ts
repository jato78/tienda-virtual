import {v4 as uuid} from 'uuid';


export class GeneroProducto {
  selected: boolean;
  descripcion: string [];
  id = uuid();

  
 constructor(public nombre: string, public imagenUrl: string, public votes: number = 0) { 
   this.descripcion = ['color', 'talla', 'material'];
  }
  setSelected(s: boolean) {
    this.selected = s;
  }
  isSelected() {
    return this.selected;
  }
  voteUp() {
    this.votes++;
  }
  voteDown() {
    this.votes--;
  }
}

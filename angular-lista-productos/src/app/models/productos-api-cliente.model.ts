import { GeneroProducto } from './genero-producto.model';
import { Subject, BehaviorSubject } from 'rxjs';

export class ProductosApiCliente{
    productos: GeneroProducto [];
    current: Subject<GeneroProducto> = new BehaviorSubject<GeneroProducto>(null);
    constructor() {
        this.productos = [];
    }
    
    add(d: GeneroProducto) {
         this.productos.push(d);
     }

     getAll(): GeneroProducto [] {
         return this.productos;
     }

     getById(id: String): GeneroProducto {
        return this.productos.filter(function(d) {return d.id.toString() === id; })[0];
     }
     elegir(d: GeneroProducto) {
        this.productos.forEach(x => x.setSelected(false));
        d.setSelected(true);
        this.current.next(d);

     }
     subscribeOnChange(fn) {
         this.current.subscribe(fn);
     }
}

     
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { GeneroProducto  } from './genero-producto.model';
import { ProductosApiCliente  } from './productos-api-cliente.model';



// ESTADO
export interface GenerosProductosStates {  
    items: GeneroProducto [];
    loading: boolean;
    favorito: GeneroProducto;
}

export const initializeGenerosProductosStates = function() {
    return {
    items: [],
    loading: false,
    favorito: null
  };
};

// ACCIONES
export enum GenerosProductosActionTypes {
  NUEVO_PRODUCTO = '[Genero Productos] Nuevo',
  ELEGIDO_FAVORITO = '[Genero Productos] Favorito',
  VOTE_UP = '[Genero Productos] vote_Up',
  VOTE_DOWN = '[Genero Productos] vote_Down'
  
}

export class NuevoProductoAction implements Action {
  type = GenerosProductosActionTypes.NUEVO_PRODUCTO;
  constructor(public producto: GeneroProducto) {}
}

export class ElegidoFavoritoAction implements Action {
  type = GenerosProductosActionTypes.ELEGIDO_FAVORITO;
  constructor(public producto: GeneroProducto) {}
}

export class VoteUpAction implements Action {
  type = GenerosProductosActionTypes.VOTE_UP;
  constructor(public producto: GeneroProducto) {}
}

export class VoteDownAction implements Action {
    type = GenerosProductosActionTypes.VOTE_DOWN;
    constructor(public producto: GeneroProducto) {}
}

export type GenerosProductosActions = NuevoProductoAction | ElegidoFavoritoAction 
| VoteUpAction | VoteDownAction;


// REDUCERS
export function reducerGenerosProductos(
  state: GenerosProductosStates,
  action: GenerosProductosActions
): GenerosProductosStates {
  switch (action.type) {
    case GenerosProductosActionTypes.NUEVO_PRODUCTO: {
      return {
          ...state,
          items: [...state.items, (action as NuevoProductoAction).producto ]
        };
    }
    case GenerosProductosActionTypes.ELEGIDO_FAVORITO: {
        state.items.forEach(x => x.setSelected(false));
        const fav: GeneroProducto = (action as ElegidoFavoritoAction).producto;
        fav.setSelected(true);
        return {
          ...state,
          favorito: fav
        };
      }
      case GenerosProductosActionTypes.VOTE_UP: {
        const d: GeneroProducto = (action as VoteUpAction).producto;
        d.voteUp();
        return {...state };
      }
      case GenerosProductosActionTypes.VOTE_DOWN: {
        const d: GeneroProducto = (action as VoteDownAction).producto;
        d.voteDown();
        return {...state };
      }
  }

  return state;
}

// EFFECTS
@Injectable()
export class GenerosProductosEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(GenerosProductosActionTypes.NUEVO_PRODUCTO),
    map((action: NuevoProductoAction) => new ElegidoFavoritoAction(action.producto))
  );

  constructor(private actions$: Actions) {}
}


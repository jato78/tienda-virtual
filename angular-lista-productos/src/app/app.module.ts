import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap, Store } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule} from '@ngrx/effects';
import { AppComponent } from './app.component';
import { GeneroProductoComponent } from './genero-producto/genero-producto.component';
import { GeneroListaComponent } from './genero-lista/genero-lista.component';
import { ProductoDetalleComponent } from './producto-detalle/producto-detalle.component';
import { ProductosApiCliente} from './models/productos-api-cliente.model';
import { GenerosProductosStates, 
          reducerGenerosProductos,  
          initializeGenerosProductosStates, 
          GenerosProductosEffects  
        } from './models/genero-producto-state.model';
import { FormGeneroProductoComponent } from './form-genero-producto/form-genero-producto.component';



const routes: Routes =[
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: GeneroListaComponent},
  {path: 'producto/:id', component: ProductoDetalleComponent},
];

//redux init
export interface AppState {
  productos: GenerosProductosStates;
}

const reducers: ActionReducerMap<AppState> = {
  productos: reducerGenerosProductos
};

let reducersInitialState = {
  productos: initializeGenerosProductosStates()
};

//redux fin init
@NgModule({
  declarations: [
    AppComponent,
    GeneroProductoComponent,
    GeneroListaComponent,
    ProductoDetalleComponent,
    FormGeneroProductoComponent
    //NgRxStoreModule
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState}),
    EffectsModule.forRoot([GenerosProductosEffects]),
    StoreDevtoolsModule.instrument()
  ],
  providers: [
    ProductosApiCliente
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

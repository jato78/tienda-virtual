import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneroProductoComponent } from './genero-producto.component';

describe('GeneroProductoComponent', () => {
  let component: GeneroProductoComponent;
  let fixture: ComponentFixture<GeneroProductoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneroProductoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneroProductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { GeneroProducto } from './../models/genero-producto.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { VoteUpAction, VoteDownAction } from '../models/genero-producto-state.model';

@Component({
  selector: 'app-genero-producto',
  templateUrl: './genero-producto.component.html',
  styleUrls: ['./genero-producto.component.css']
})
export class GeneroProductoComponent implements OnInit {
  @Input() producto: GeneroProducto;
  @Input('idx') position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<GeneroProducto> 
 
  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();

  }

  ngOnInit() {
  }

  ir() {
    this.clicked.emit(this.producto);
    
    return false;
  }
  voteUp() {
    this.store.dispatch(new VoteUpAction(this.producto));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.producto));
    return false;
  }
}
